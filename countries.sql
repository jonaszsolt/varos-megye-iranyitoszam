-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 27, 2017 at 12:33 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ceglista`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Bács-Kiskun', NULL, NULL),
(2, 'Baranya', NULL, NULL),
(3, 'Békés', '2017-07-13 10:58:06', NULL),
(4, 'Borsod-Abaúj-Zemplén', '2017-07-13 10:58:06', NULL),
(5, 'Csongrád', '2017-07-13 10:59:11', NULL),
(6, 'Fejér', '2017-07-13 10:59:11', NULL),
(7, 'Győr-Moson-Sopron', '2017-07-13 10:59:11', NULL),
(8, 'Hajdú-Bihar', '2017-07-13 10:59:11', NULL),
(9, 'Heves', '2017-07-13 10:59:11', NULL),
(10, 'Jász-Nagykun-Szolnok', '2017-07-13 11:00:11', NULL),
(11, 'Komárom-Esztergom', '2017-07-13 11:00:11', NULL),
(12, 'Nógrád', '2017-07-13 11:00:11', NULL),
(13, 'Pest', '2017-07-13 11:00:11', NULL),
(14, 'Somogy', '2017-07-13 11:00:11', NULL),
(15, 'Szabolcs-Szatmár-Bereg', '2017-07-13 11:01:29', NULL),
(16, 'Tolna', '2017-07-13 11:01:29', NULL),
(17, 'Vas', '2017-07-13 11:01:29', NULL),
(18, 'Veszprém', '2017-07-13 11:01:29', NULL),
(19, 'Zala', '2017-07-13 11:01:29', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
