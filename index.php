<?php 
        //Iranyitoszam_telepules_posta.csv
        //telepulesesmegye.csv



        $megyek = \App\Country::all()->lists('id', 'name');
        // name => id

        /* 0 => "21-es major (Kamaráspuszta)"
          1 => "Békés" */
        $f_array1 = explode("\n", Storage::disk('public')->get('telepulesesmegye.csv'));
        $helysegmegye = array_map(function($d) {
            return str_getcsv(trim($d), ";");
        }, $f_array1);


        $helyseg_megye = [];
        foreach ($helysegmegye as $r) {
            if (isset($r[0]) && isset($r[1])) {
                $helyseg_megye[$r[0]] = $r[1];
            }
        }


        /* 0 => "2099"
          1 => "Pilisszentkereszt"
          2 => "Dobogókő"? */
        //$file = Storage::disk('public')->get('Iranyitoszam_telepules_posta.csv');
        $f_array = explode("\n", Storage::disk('public')->get('Iranyitoszam_telepules_posta.csv'));
        $iranysz_helyseg_telepulesresz = array_map(function($d) {
            return str_getcsv($d, ";");
        }, $f_array);

        foreach ($iranysz_helyseg_telepulesresz as $row) {
            
            $notvalid = [];

            if (isset($row[0]) && isset($row[1]) && isset($row[2])) {
                $row[0] = trim($row[0]);
                $row[1] = trim($row[1]);
                $row[2] = trim($row[2]);

                $data = [];
                $data['postcode'] = $row[0];


                if (!empty($row[2])) {
                    $data['name'] = $row[2].' ('.$row[1].')';

                    if (isset($helyseg_megye[$row[2]])) {
                        $megyenev = $helyseg_megye[$row[2]];
                        $data['country_id'] = $megyek[$megyenev];
                    }
                } else if (!empty($row[1])) {
                    $data['name'] = $row[1];

                    if (isset($helyseg_megye[$row[1]])) {
                        $megyenev = $helyseg_megye[$row[1]];
                        $data['country_id'] = $megyek[$megyenev];
                    }
                }
                City::create($data);
            }else{
                $notvalid[] = $row;
            }

        }

        echo '<pre>';
              print_r($notvalid);
              echo '</pre><br>'; 
?>
